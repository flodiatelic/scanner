
var noble = require('noble');

var knownsBeacons = [];


noble.on('stateChange', function(state) {
  if (state === 'poweredOn')
    noble.startScanning([], true);
  else
    noble.stopScanning();
});

function calculateDistance(rssi) {
  
  var txPower = -62 //hard c coded power value. Usually ranges between -59 to -65
  
  if (rssi == 0) {
    return -1.0; 
  }

  var ratio = rssi*1.0/txPower;
  if (ratio < 1.0) {
    return Math.pow(ratio,10);
  }
  else {
    var distance =  (0.89976)*Math.pow(ratio,7.7095) + 0.111;    
    return distance;
  }


} 

noble.on('discover', function(peripheral) { 

  var macAdress = peripheral.uuid;
  var rss = peripheral.rssi;
  var localName =peripheral.advertisement.localName; 

      console.log('found device: ', macAdress, ' ', localName, ' ', calculateDistance(rss));
});
