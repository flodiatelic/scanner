/**
 * Created by root on 10/10/16.
 */

var bleno = require('bleno');

console.log('bleno - iBeacon');

bleno.on('stateChange', function(state) {
    console.log('on -> stateChange: ' + state);

    if (state === 'poweredOn') {
        bleno.startAdvertisingIBeacon('b9407f30f5f8466eaff925556b57fe6d', 0, 0, -59);
    } else {
        bleno.stopAdvertising();
    }
});

bleno.on('advertisingStart', function() {
    console.log('on -> advertisingStart');
});

bleno.on('advertisingStop', function() {
    console.log('on -> advertisingStop');
});