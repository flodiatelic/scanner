//Parameters
// Used to compute a Slippery average
var history={};

//Imports
var noble = require('noble');
var bleno = require('bleno');
var KalmanFilter = require('kalmanjs').default;
var io = require('socket.io-client');
var os = require('os');



var uuid = 'e2c56db5dffb48d2b060d0f5a71096e0';
var major = 0; // 0x0000 - 0xffff
var measuredPower = -59; // -128 - 127


var timeAverage = 20; // in second

var interfaces = os.networkInterfaces();
var idStation = "";
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal && address.address.substring(0,9) === "192.168.2") {
            idStation = address.address.substring(10);
        }
    }
}
console.log("My station Id = " + idStation);
var connected = false;

var io = io('http://192.168.2.1:3001');
io.on("connect", function(){
    io.emit("checkin", idStation);
})
    noble.on('discover', function(peripheral){
        if(peripheral.uuid == uuid){
            var distance = calculateDistance(rssi, peripheral.txPower);
            console.log(peripheral.minor + " : " + peripheral.txPower + " distance : " + distance);
        }
	    if(peripheral.uuid == "e3c40f05d386"){
	        console.log(peripheral.rssi);
	    }
	    if(!history[peripheral.uuid]){
		    history[peripheral.uuid] = [];
	    }
	    history[peripheral.uuid].push({ rssi: peripheral.rssi, date: Math.round(new Date().getTime()/1000)});
        deleteTooOldValues(peripheral.uuid);
        io.emit('bleDiscovered', {
            idStation: idStation,
            peripheral: {
                mac: peripheral.uuid,
                major: peripheral.advertisement.major,
                minor: peripheral.advertisement.minor,
                rssi: slipperyAverage(peripheral.uuid) * 1.1,
                name: peripheral.advertisement.localName,
                txPower: peripheral.advertisement.txPowerLevel,
                services: peripheral.advertisement.serviceUuids,
                serviceDatas: peripheral.advertisement.serviceData
            }
        });
    });

noble.on('stateChange', function startOrStopScan (state){
    if (state === 'poweredOn'){
        noble.startScanning([], true);
    }
    else{
        noble.stopScanning();
    }
});

io.on('disconnect', function() {
    connected = false;
});

function slipperyAverage (uuid){
	var somme = history[uuid].reduce(function(previous, object){
		return previous + parseFloat(object.rssi);
	}, 0);
	var length =  history[uuid].length;
	return somme/length;
}

function slipperykamlan (uuid){

	var kalmanFilter = new KalmanFilter({R: 0.1, Q: 0});
	var somme = history[uuid].map(function(v) {
		return kalmanFilter.filter(v);
	}).reduce(function(previous, object){
		return previous + parseFloat(object.rssi);
	}, 0);
	var length =  history[uuid].length;
	return somme/length;;
}

function deleteTooOldValues (uuid){
	var i = history[uuid].length - 1;
	var date = Math.round(new Date().getTime()/1000);
	while (i >= 0) {
		if (history[uuid][i] && history[uuid][i].date < date - timeAverage) {
			history[uuid].splice(i,1);
		}
		--i;
	}
}



function calculateDistance(rssi, txPower) {
    txPower = txPower || -42 //hard c coded power value. Usually ranges between -59 to -65

	if (rssi == 0) {
		return -1.0;
	}
	var ratio = rssi*1.0/txPower;
	return (0.89976)*Math.pow(ratio,7.7095) + 0.111;

}
/**




bleno.on('stateChange', function startOrStopScan (state){
	if (state === 'poweredOn'){

		bleno.startAdvertisingIBeacon(uuid, major, idStation, measuredPower);
	}
	else{
		bleno.stopAdvertisingIBeacon();
	}
});**/